from django.apps import AppConfig


class RaspiBlogConfig(AppConfig):
    name = 'raspi_blog'

from django.db import models
from django.db.models import permalink
from django.contrib.auth.models import User
import os

def my_upload_function(instance, filename):
    return os.path.join('images/%s/%s' % (instance.post.id, filename ))

class Post(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, db_index=True)
    image = models.ImageField(upload_to="thumbnails")
    introduction = models.TextField()
    body = models.TextField()
    posted = models.DateTimeField(auto_now_add = True) 
    published = models.BooleanField(default=True)
    author = models.ForeignKey(User)
    categories = models.ManyToManyField('raspi_blog.Category')

    def __str__(self):
        return self.title
    

class Category(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=100, db_index=True)
    
    def __str__(self):
        return self.title
    


class Comment(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    posted = models.DateTimeField(auto_now_add=True)
    comment = models.TextField()
    ip_address = models.CharField(max_length=100)
    post = models.ForeignKey('raspi_blog.Post')
    
    def __str__(self):
        return str(self.post.id) + "_" + self.post.title + " "  + str(self.posted)
        
        
class PostImage(models.Model):
    image = models.ImageField(upload_to= my_upload_function)
    post = models.ForeignKey('raspi_blog.Post')
    
    def __str__(self):
        return str(self.post.id) + "_" + self.post.title + "_" + str(self.id)
    


    
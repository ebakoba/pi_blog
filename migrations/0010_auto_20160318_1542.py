# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-18 15:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('raspi_blog', '0009_auto_20160318_1532'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='image',
            field=models.ImageField(upload_to='', verbose_name='images'),
        ),
    ]

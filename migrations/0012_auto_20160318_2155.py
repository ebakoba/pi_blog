# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-03-18 21:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('raspi_blog', '0011_auto_20160318_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='image',
            field=models.ImageField(upload_to='images/'),
        ),
    ]

from django.shortcuts import render, get_object_or_404, redirect
from .models import Post, Category, Comment, PostImage
from .forms import CommentForm

def search(request, search='', select=1):
    select = int(select)
    
    if request.method == "POST":    
        search_words = request.POST.get('search').split(' ')
        search_sentence = ''
        for word in search_words:
            if search_words.index(word) == 0:
                search_sentence = word
            else:
                search_sentence = search_sentence + '-' + word
        return redirect('/otsing/' + search_sentence )
    
    search_words = search.split('-')
    for word in search_words:
        if search_words.index(word) == 0:
            search = word
        else:
            search = search + ' ' + word
            
    
    
    if len(search.split(' ')) < 2:
        posts = (Post.objects.filter(title__contains=search, published=True) | 
                 Post.objects.filter(categories__title__contains=search, published=True) | 
                 Post.objects.filter(categories__slug__contains=search, published=True) |
                 Post.objects.filter(introduction__contains=search, published=True) |
                 Post.objects.filter(author__first_name__contains=search, published=True)|
                 Post.objects.filter(author__last_name__contains=search, published=True).order_by('-id'))
    else :
        posts = (Post.objects.filter(title__contains=search, published=True) |
                 Post.objects.filter(categories__title__contains=search, published=True) |
                 Post.objects.filter(categories__slug__contains=search, published=True) |
                 Post.objects.filter(author__first_name__contains=search.split(' ')[0], author__last_name__contains=search.split(' ')[1], published=True) |
                 Post.objects.filter(author__first_name__contains=search.split(' ')[1], author__last_name__contains=search.split(' ')[0], published=True) |
                 Post.objects.filter(introduction__contains=search, published=True).order_by('-id'))
    
    posts = list(set(posts))

    if not posts[((select)*5):((select + 1)*5)]:
        next = False
    else:
        next = True
    posts = posts[((select-1)*5):((select)*5)]
    
    if len(Category.objects.filter(slug__contains=search)) > 0:
        search = Category.objects.filter(slug__contains=search)[0].title
        
    return render(request, 'raspi_blog/home.html',{
        'posts': posts,
        'categories': Category.objects.all(),
        'search': search,
        'select': select,
        'next': next,
        'is_search': True,})

    
    
    
def show_posts(request, select=1):
    select = int(select)
    posts = Post.objects.filter(published=True).order_by('-id')
    if not posts[((select)*5):((select + 1)*5)]:
        next = False
    else:
        next = True
    return render(request, 'raspi_blog/home.html', {
        'categories': Category.objects.all(),
        'posts': posts[((select-1)*5):((select)*5)],
        'select': select,
        'next': next,
        'is_home': True,
    })
    
    
    
    
def view_post(request, pk, slug):
    post = get_object_or_404(Post.objects.filter(published=True), pk=pk)
    comments = Comment.objects.filter(post=post)
    images = PostImage.objects.filter(post=post)
    comment_form = CommentForm()
    
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            
            x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
            if x_forwarded_for:
                ip = x_forwarded_for.split(',')[0]
            else:
                ip = request.META.get('REMOTE_ADDR')
            
            if ip == None:
                ip = "unknown"
                
            comment.ip_address = ip
            comment.save()
    
    return render(request, 'raspi_blog/post.html', {
        'post': post,
        'comments': comments.order_by('-id'),
        'comment_form': comment_form,
        'images': images,
        'categories': Category.objects.all(),
    })
    
def view_contact(request):
    return render(request, 'raspi_blog/contact.html', {
        'categories': Category.objects.all(),
    })

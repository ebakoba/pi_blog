from django.utils.translation import ugettext_lazy as _
from django.forms import ModelForm, TextInput, Textarea, CharField, Form
from .models import Comment
from captcha.fields import CaptchaField

class CommentForm(ModelForm):
    captcha = CaptchaField()
    class Meta:
        model = Comment
        fields = ['name','email', 'comment']
        widgets = {
            'name': TextInput(attrs={'placeholder': ' Nimi',
                                      'class': 'form-control'}),
            'email': TextInput(attrs={'placeholder': ' E-mail',
                                      'class': 'form-control'}),
            'comment': Textarea(attrs={'style': 'width:100%;height:100px;resize:none', 
                                      'class': 'form-control'}),
        }
        
class SearchForm(Form):
    search = CharField(max_length=100)
    
from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^(?P<select>\d+)$', views.show_posts, name="show_posts"),
    url(r'^$', views.show_posts, name="show_posts"),
    url(r'^(?P<select>\d+)/otsing/(?P<search>[\w.@+-]+)$', views.search, name="search"),
    url(r'^(?P<select>\d+)/otsing/$', views.search, name="search"),
    url(r'^otsing/(?P<search>[\w.@+-]+)$', views.search, name="search"),
    url(r'^otsing/$', views.search, name="search"),
    url(r'^(?P<pk>\d+)/(?P<slug>[\w.@+-]+)/$', views.view_post, name="view_post"),
    url(r'^kontakt$', views.view_contact, name="view_contact"),
]